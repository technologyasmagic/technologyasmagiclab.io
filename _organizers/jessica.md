---
name: Jessica Hammer
order: 9
portrait: /images/jessica_hammer.jpg
affiliation: Carnegie Mellon University
website: http://replayable.net/
---

Jessica Hammer is the Thomas and Lydia Moran Assistant Professor of Learning Science at Carnegie Mellon University and an award-winning role-playing game designer.