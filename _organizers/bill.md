---
name: Bill Hamilton
order: 7
portrait: /images/bill_profile_square.png
affiliation: PLEX Lab | New Mexico State University
website: https://www.cs.nmsu.edu/plexlab/people/bill/
---

Bill is an Assistant Professor of Computer Science at New Mexico State University, where he directs the Participatory Live Experiences Lab. Bill’s work investigates the formation of live media communities and how the design of media technologies can impact participation in situated play, education, and political contexts. Bill also investigates the design of collaborative games.