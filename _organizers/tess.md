---
name: Theresa J. Tanenbaum
order: 4
portrait: /images/tess.jpg
affiliation: Transformative Play Lab, University of California-Irvine
website: https://transformativeplay.ics.uci.edu/Tess-Tanenbaum/
---

Theresa J. Tanenbaum is an Assistant Professor of Informatics at UC Irvine. She studies identity and role-playing in XR digital theater experiences.