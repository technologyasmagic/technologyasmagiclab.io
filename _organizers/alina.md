---
name: Alina Striner
order: 1
portrait: /images/alina.png
affiliation:  Centrum Wiskunde & Informatica (CWI)
website: http://alina.striner.com/
---

Alina Strineris a Postdoctoral ERCIM fellow at the Centrum Wiskunde & Informatica (CWI). She researches audience participation design in theater, games, & transmedia.