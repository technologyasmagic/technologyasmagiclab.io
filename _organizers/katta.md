---
name: Katta Spiel
order: 3
portrait: /images/extermikate.jpg
affiliation: TU Wien (Vienna University of Technology)
website: http://katta.mere.st
---

Katta Spiel researches marginalised perspectives in interaction design, often 
with a focus on playful engagements at TU Wien. Katta's most recent work 
centered on the experiences of autistic children with technologies and including 
their first-hand perspectives. They also have several years of experience as a 
game designer for the collaboratively created and maintained *[Discworld MUD](http://discworld.starturtle.net)*.