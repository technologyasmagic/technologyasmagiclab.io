---
name: Z Toups
order: 8
portrait: /images/zprofile.jpg
affiliation: PIxL Lab | New Mexico State University
website: https://pixl.nmsu.edu/people/z/
---

Z is an Associate Professor of Computer Science at New Mexico State University, where they direct the Play & Interactive Experiences for Learning Lab. Z's work is primarily concerned with collaboration in game interfaces, looking at how groups of players plan strategy and communicate tactics. They apply this work to wearable computer systems and mixed realities. Z is *well played* in digital games and tabletop RPGs and board games.