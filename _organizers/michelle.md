---
name: Michelle Cormier
order: 5
portrait: /images/michelle.png
affiliation: PIxL Lab | New Mexico State University
---

Michelle Cormier is a graduate student in Computer Science at New Mexico University. She is researching human interaction with digital agents and mixed reality environments.