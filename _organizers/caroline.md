---
name: Caroline Pitt
order: 6
portrait: /images/caroline.jpg
affiliation: The Information School, University of Washington
---

Caroline Pitt is a PhD Candidate in Information Science at the University of Washington. She studies the design of sociotechnical systems for informal learning.
