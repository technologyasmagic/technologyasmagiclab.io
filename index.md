---
layout: default
title: Call for Participation
---

## Call for Participation
If you are excited, curious, and/or interested in how table-top role-playing games can be used in technology design, this workshop is for you! Tabletop role-playing games are becoming increasingly popular across physical and digital mediums, engaging players in collaborative storytelling. These games can help designers explore and reflect on technology design choices. 

In this one-day workshop, we will cast various technologies as magic in a role-playing fantasy game world, and play tabletop role-playing games using this metaphor. After play, we will reveal what technologies were represented as magic, and reflect on personal attitudes, practices, and values that participants encountered during play. We will use these experiences to consider redesign opportunities for technology, and discuss the value of tabletop role-playing games as a critical reflection tool. As an outcome to the workshop, we expect to develop new research and teaching guides to involve tabletop role-playing games in design methods. 

#### Submissions

To participate, submit the following by [email](mailto:{{ site.email }}?subject=Position%20Paper%20Submission%20for%20CHI%20Workshop%20on%20Technology%20As%20Magic):
1. A description of your experience with tabletop role-playing games
2. A personal reflection on a memorable experience involving technology
3. A short narrative that develops technology into a magical item, entity, or effect in a fictional setting. In advance of the workshop, we will group together these narratives and work with participants to develop characters that they will play during the workshop.

#### Important Dates

{% include dates.md %}



